#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, List
import math
from MetaCacheVar import CACHE

# ------------
# collatz_read
# ------------


def collatz_read(s: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]


# ------------
# collatz_eval
# ------------


def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """

    assert i > 0 and j > 0

    if i > j:
        i, j = j, i

    # The range of indexes in the metacache to check
    cache_start_idx = math.ceil((i / 1000))
    cache_end_idx = math.floor((j / 1000))

    max_cycle = 0

    # The first and last values we don't have to manually check
    cache_start_num = (cache_start_idx * 1000) + 1
    cache_end_num = cache_end_idx * 1000

    if j - i < 1000:
        # The metacache can't be used since the values are contained in one range
        # We use the basic solution as is
        for k in range(i, j + 1):
            collatz_cycle = find_cycle(k)
            if collatz_cycle > max_cycle:
                max_cycle = collatz_cycle
    else:

        # Use the basic solution to calculate the values before the start
        # of the cache's range
        pre_vals = i
        while pre_vals < cache_start_num:
            collatz_cycle = find_cycle(pre_vals)
            if collatz_cycle > max_cycle:
                max_cycle = collatz_cycle
            pre_vals += 1

        # Use the metacache to skip calculation for ranges in increments of 1000
        # between i and j where possible
        while cache_start_idx < cache_end_idx:
            if CACHE[cache_start_idx] > max_cycle:
                max_cycle = CACHE[cache_start_idx]
            cache_start_idx += 1

        # Use the basic solution to calculate the values after the end
        # of the cache's range
        post_vals = cache_end_num
        while post_vals <= j:
            collatz_cycle = find_cycle(post_vals)
            if collatz_cycle > max_cycle:
                max_cycle = collatz_cycle
            post_vals += 1

    assert max_cycle > 0

    return max_cycle


def find_cycle(k: int) -> int:
    """
    Helper function that actually calculates the cycle length for a given number
    k, the number we want to calculate Collatz cycle of
    return the cycle length of k
    """
    assert k >= 1

    cycles = 1
    while k != 1:
        if k % 2 != 0:
            k = (3 * k) + 1
            cycles += 1
        else:
            k = k // 2
            cycles += 1

    assert cycles >= 1

    return cycles


# -------------
# collatz_print
# -------------


def collatz_print(w: IO[str], i: int, j: int, v: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
